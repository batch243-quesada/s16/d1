// console.log('Hello World');

// [Section] Arithmetic Operators
//  + - * / %  ** ++ --

let x = 25;
let y = 111;
let z = x % y;
let w = "John"
console.log(z); // returns 25;

// [Section] Assignment Operators
// += -= *= /= %= **= 

// [Section] Incrementation and Decrementation
let n = 1;

//Pre-increment
let increment = ++n;
console.log(`result of n in Pre-increment: ${n}`); // returns 2
console.log(`result of increment in pre-increment: ${increment}`); // returns 2

//Post-increment
increment = n++;
console.log(`result of n in post-increment: ${n}`); // returns 3
console.log(`result of increment in post-increment: ${increment}`); // returns 2

// [Section] Type Coercion
// e.g. String + integer = String;
// "20" + 22 = 2022;

let r = x + w;
console.log(r); // returns 25John
console.log(typeof r); // returns string

// Boolean + integer = integer;
let result1 = true + 20;
let result2 = false +20;
console.log(result1); // returns 21
console.log(result2); // returns 20


// [Section] Comparison Operators
// == >= <= 
let isEqual = 1 == 1; // will return true;
console.log(isEqual);

console.log( 1 == '1'); // returns true

// Strict comparison
console.log( 1 === '1'); // returns false 

let numStr = '30';
console.log(x > numStr); // returns false


let i = 1;
console.log(i++); // will log 1 out to the console.

// Post increment
let o = 1;
o++; // <----- Post increment
console.log(o); // will log 2 out to the console.


// Pre increment
let p = 1;
console.log(++p); // will log 2 out to the console.

			// EXAMPLE
			let a = 1;
			let b;

			b = a++; // Since a++ is a post-increment, j will fetch the initial value of a which is 1 only.

			console.log(b, a); // So j here is equal to 1 and a is 2.

			// on the other hand
			let c = 1;
			let d;

			d = ++c; // Pre increment

			console.log(d, c) // will log 2 and 2 out to the console.

